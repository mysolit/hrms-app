import Iconx from "../../components/atoms/icons/iconx";
import { sidebarConfigProps } from "../types";

const getIcon = (name: string) => <Iconx icon={name} className="w-5 h-5 " />;

const ICONS = {
  dashboard: getIcon("BuildingLibraryIcon"),
  employee: getIcon("UserGroupIcon"),
};

const sidebarConfig = (
  isAdmin?: boolean
): {
  subHeader: string;
  items: {
    title: string;
    path: string;
    icon: JSX.Element;
    children?: {
      title: string;
      path: string;
    }[];
  }[];
}[] => [
  {
    subHeader: "General",
    items: [
      {
        title: "Dashboard",
        path: "/",
        icon: ICONS.dashboard,
      },
    ],
  },
  {
    subHeader: "Management",
    items: [
      {
        title: "Employee",
        path: "/employee",
        icon: ICONS.employee,
        children: [
          {
            title: "Roles",
            path: "/employee/roles",
          },
          {
            title: "Permissions",
            path: "/employee/permissions",
          },
        ],
      },
      {
        title: "Employee 2",
        path: "/employee",
        icon: ICONS.employee,
        children: [
          {
            title: "Roles",
            path: "/employee/roles",
          },
          {
            title: "Permissions",
            path: "/employee/permissions",
          },
        ],
      },
    ],
  },
];
export default sidebarConfig;
