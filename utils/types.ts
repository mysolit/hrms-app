export interface LayoutProps {
  children: JSX.Element | JSX.Element[];
}

/**
 * Sidebar Props
 */
export interface sidebarConfigProps {
  subHeader: string;
  items: {
    title?: string;
    path?: string;
    icon?: JSX.Element;
    children?: {
      title?: string;
      path?: string;
    }[];
  }[];
}
[];

export type NavItemProps = {
  title: string;
  path: string;
  icon: JSX.Element;
  info?: JSX.Element;
  children?: {
    title: string;
    path: string;
  }[];
};

export interface NavSectionProps {
  navConfig: {
    subHeader: string;
    items: NavItemProps[];
  }[];
}
