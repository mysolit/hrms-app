/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./app/**/*.{js,ts,jsx,tsx}",
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        ruby: "#29D882",
        "secondary-ruby": "#00BD95",
        CetaceanBlue: "#051345",
        CoolBlack: "#092C62",
        pepperoncini: "#dbc851",
        "radar-blip-green": "#9BEF7F",
        monarchist: "#5064D3",
        "coral-silk": "#F2A57B",
      },
    },
  },
  plugins: [],
};
