import { NavItemProps, NavSectionProps } from "../../../utils/types";
import { NavItem } from "../../atoms/layouts/sidebarNavItem";

export default function NavSection({
  navConfig,
  ...other
}: NavSectionProps): JSX.Element {
  return (
    <div className="flex-auto overflow-y-auto">
      {navConfig.map((list) => {
        const { subHeader, items } = list;
        return (
          <div className="mb-4" key={subHeader}>
            <h1 className={`px-4 py-2 `}>{subHeader}</h1>
            {items.map((item: NavItemProps, i: number) => (
              <NavItem key={i} item={item} />
            ))}
          </div>
        );
      })}
    </div>
  );
}
