import { NextPage } from "next";
import Image from "next/image";
import { LayoutProps } from "../../../utils/types";
import Header from "./header";
import Sidebar from "./sidebar";

export const Layout: NextPage<LayoutProps> = ({ children }) => {
  return (
    <div className="fixed w-full h-screen">
      <div className="flex flex-row overflow-hidden">
        <Sidebar />
        <div className="relative flex flex-col flex-1 overflow-x-hidden overflow-y-auto">
          {/* Header */}
          <Header />

          <main className="h-screen overflow-y-auto bg-neutral-200">
            <div className="w-full px-4 py-8 mx-auto sm:px-6 lg:px-8 max-w-9xl">
              {children}
            </div>
          </main>
        </div>
      </div>
    </div>
  );
};
