import { NextPage } from "next";
import Image from "next/image";
import sidebarConfig from "../../../utils/data/sidebarConfig";
import NavSection from "../../molecules/navSection";

const Sidebar: NextPage = () => {
  return (
    <div className="flex flex-col flex-none border-r bg-neutral-200 border-neutral-300 w-72">
      <div className="flex-none w-full px-4 mb-6">
        <div className="w-10 h-10 my-5">
          <Image
            src={"/images/MYSOL-03.jpg"}
            width={4500}
            height={4500}
            alt="Atobarai_logo_"
            className="rounded-full"
          />
        </div>
        <div className="flex items-center p-4 space-x-4 rounded-lg shadow-lg bg-slate-300">
          <div className="w-16 h-16 rounded-full ">
            <Image
              src={"/images/minimal_avatar.jpg"}
              width={240}
              height={240}
              alt="minimal_avatar"
              className="rounded-full "
            />
          </div>
          <div className="">
            <h1 className="text-lg ">User Name</h1>
            <p className="text-xs ">Admin</p>
          </div>
        </div>
      </div>
      <NavSection navConfig={sidebarConfig()} />
    </div>
  );
};
export default Sidebar;
