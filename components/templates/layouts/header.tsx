import { Popover, Transition } from "@headlessui/react";
import { NextPage } from "next";
import Image from "next/image";
import { Fragment } from "react";
import Iconx from "../../atoms/icons/iconx";

const Header: NextPage = () => {
  return (
    <div className="sticky top-0 z-30 h-20 bg-neutral-200">
      <div className="h-full px-4 sm:px-6 lg:px-8 ">
        <div className="flex flex-row-reverse items-center h-full">
          <div className="flex items-center ">
            <div className="flex items-center space-x-6">
              <Iconx icon="MagnifyingGlassCircleIcon" className="w-8 h-8 " />
              <Iconx
                icon="ChatBubbleOvalLeftEllipsisIcon"
                className="w-8 h-8 "
              />
            </div>

            {/* User Dropdown */}
            <Popover className={"relative ml-10"}>
              {({ open }) => (
                <>
                  <Popover.Button className={`focus:outline-none`}>
                    <div className="w-12 h-12 rounded-full ">
                      <Image
                        src={"/images/minimal_avatar.jpg"}
                        width={240}
                        height={240}
                        alt="minimal_avatar"
                        className="rounded-full "
                      />
                    </div>
                  </Popover.Button>
                  <Transition
                    as={Fragment}
                    enter="transition ease-out duration-200"
                    enterFrom="opacity-0 translate-y-1"
                    enterTo="opacity-100 translate-y-0"
                    leave="transition ease-in duration-150"
                    leaveFrom="opacity-100 translate-y-0"
                    leaveTo="opacity-0 translate-y-1">
                    <Popover.Panel
                      className={
                        "absolute -left-12 z-30 mt-1  max-w-sm -translate-x-1/2 transform px-4 sm:px-0 lg:max-w-sm"
                      }>
                      <div className="overflow-hidden rounded-lg shadow-lg bg-slate-200">
                        <div className="px-3 py-4 mb-1 border-b border-slate-400">
                          <div className="font-medium text-slate-800">
                            Arkar Phyo
                          </div>
                          <div className="text-xs italic text-slate-500">
                            michaelzhong833@gmail.com
                          </div>
                        </div>
                        <div className="px-3 py-4">
                          <button className="w-full px-4 py-1 border rounded-lg shadow-lg border-slate-500">
                            Signout
                          </button>
                        </div>
                      </div>
                    </Popover.Panel>
                  </Transition>
                </>
              )}
            </Popover>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Header;
