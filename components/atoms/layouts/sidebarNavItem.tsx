import { NavItemProps } from "../../../utils/types";
import { Disclosure, Transition } from "@headlessui/react";
import Link from "next/link";
import Iconx from "../icons/iconx";
import { useRouter } from "next/router";

export const NavItem = ({ item }: { item: NavItemProps }): any => {
  const { title, path, icon, children } = item;
  const router = useRouter();
  const { pathname } = router;
  return (
    <>
      {children && children ? (
        <div className="mb-3 ">
          <Disclosure as="div">
            {({ open }) => (
              <>
                <Disclosure.Button
                  className={`${
                    pathname === path
                      ? " bg-ruby/20 border-r-2 border-secondary-ruby text-secondary-ruby"
                      : ""
                  } flex items-center justify-between w-full px-4 py-2 text-sm font-medium text-left focus:outline-none hover:bg-gray-300`}>
                  <div className="flex ml-6 space-x-3">
                    <span>{icon}</span>
                    <span>{title}</span>
                  </div>
                  <Iconx
                    icon="ChevronRightIcon"
                    className={`${open ? "rotate-90 transform" : ""} h-5 w-5`}
                  />
                </Disclosure.Button>
                <Transition
                  enter="transition duration-100 ease-out"
                  enterFrom="transform scale-95 opacity-0"
                  enterTo="transform scale-100 opacity-100"
                  leave="transition duration-75 ease-out"
                  leaveFrom="transform scale-100 opacity-100"
                  leaveTo="transform scale-95 opacity-0">
                  <Disclosure.Panel className={`  text-slate-700`}>
                    {children.map((cItem) => {
                      const { title: cTitle, path: cPath } = cItem;
                      return (
                        <Link
                          href={cPath}
                          className={`${
                            pathname === cPath ? "  text-secondary-ruby" : ""
                          } flex items-center w-full px-4 py-2 text-sm group font-medium text-left focus:outline-none hover:bg-gray-300`}>
                          <div className="flex items-center ml-8 space-x-4">
                            <p
                              className={`${
                                pathname === cPath
                                  ? " bg-secondary-ruby w-1.5 h-1.5"
                                  : "bg-slate-700 w-1 h-1"
                              }  rounded-full `}
                            />
                            <p> {cTitle}</p>
                          </div>
                        </Link>
                      );
                    })}
                  </Disclosure.Panel>
                </Transition>
              </>
            )}
          </Disclosure>
        </div>
      ) : (
        <div
          className={`${
            pathname === path
              ? " bg-ruby/20 border-r-2 border-secondary-ruby text-secondary-ruby"
              : ""
          } py-2 font-medium`}>
          <Link className={`px-4 ml-6`} href={path}>
            {title}
          </Link>
        </div>
      )}
    </>
  );
};
